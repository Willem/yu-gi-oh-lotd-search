var search = instantsearch({
    appId: 'SLCAGNZAWC',
    apiKey: '0239104523e0a0db6098030fa71461f6',
    indexName: 'yugioh',
    urlSync: {}
  });
  
  search.addWidget(
    instantsearch.widgets.searchBox({
      container: '#q',
      poweredBy: true,
      autoFocus: true,
      searchOnEnterKeyPressOnly: true
    })
  );
  
  search.addWidget(
    instantsearch.widgets.stats({
      container: '#stats'
    })
  );
  
  var hitTemplate = document.querySelector('#hit-template').innerHTML;
  
  var noResultsTemplate =
    '<div class="text-center">No results found matching <strong>{{query}}</strong>.</div>';
  
  search.addWidget(
    instantsearch.widgets.hits({
      container: '#cards',
      hitsPerPage: 10,
      templates: {
        empty: noResultsTemplate,
        item: hitTemplate
      },
      transformData: function(card) {
          if (card.Level > 0) {
              card.hasLevel = true;
              card.isCreature = true;
          }
          else if (card.Rank > 0) {
              card.hasRank = true;
              card.isCreature = true;
          }
  
          card.hasWiki = !!card.WikiUrl
  
          return card;
      }
    })
  );
  
  search.addWidget(
    instantsearch.widgets.pagination({
      container: '#pagination',
      cssClasses: {
        root: 'pagination',
        active: 'active'
      }
    })
  );
  
  search.addWidget(
    instantsearch.widgets.refinementList({
      container: '#property',
      attributeName: 'Properties',
      operator: 'and',
      limit: 10,
      searchForFacetValues: true,
      cssClasses: {
        list: 'nav nav-list',
        count: 'badge pull-right',
        active: 'active'
      }
    })
  );
  
  search.start();